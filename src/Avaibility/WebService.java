package Avaibility;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import Properties.Configuration;

/**
 * WebService.java
 * Purpose: Request server, if the responseCode is fine return true.
 *
 * @author Tommy Vinhas
 * @version 1.0 05/12/2013
 */

public class WebService {
	
	public static String response;
	
    /** 
     * Send a request to "WEBSERVICE_URL" and read the input stream.
     * 
     * @return A Boolean (default: true), false if the web service is  unavailable.
     */
	public static boolean sendRequest (){	  
		try {
				long startTime = System.currentTimeMillis();
				URL obj = new URL(Configuration.WEBSERVICE_URL);
				HttpURLConnection urlConnection = (HttpURLConnection) obj.openConnection();
				urlConnection.connect();
				
				if(urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
					System.out.println(urlConnection.getResponseCode());
					long elapsedTime = System.currentTimeMillis() - startTime;
					System.out.println("Response_time: " + (elapsedTime) + "ms");
					InputStream inputStream = urlConnection.getInputStream();
					InputStreamReader streamReader = new InputStreamReader(inputStream);
					
					getResponse(streamReader);
					
					return true;
				}else {
					System.out.println("404");
				}
		} 
		catch(Exception e) {
			System.out.println("404");
		}
		return false;
	} 

    /**
     * Get and deserialize the response from the input stream.
     * Return an Mquestion object.
     * 
     * @warning Only call if "responseCode = HTTP_OK".
     * @param streamReader Stream reader from input stream.
     * @return 
     */
	private static void getResponse(InputStreamReader streamReader) throws IOException {
		
		int bufferSize;
		char[] buffer = new char[1024];
		
		StringBuilder resultBuilder = new StringBuilder();
		
		while((bufferSize = streamReader.read(buffer)) > 0) {
			resultBuilder.append(buffer, 0, bufferSize);
		}
		
		response = resultBuilder.toString();
	}
}