package Avaibility;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TimerTask;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import Entity.Question;
import Properties.Configuration;
import ReportServer.EmailController;

/**
 * AvailabilityController.java
 * Purpose: Check web service availability and JSON validity,
 * if the web service is unreachable or the JSON not valid, he sends mail
 * to an administrator".
 *
 * @author Tommy Vinhas
 * @version 1.0 05/12/2013
 */

public class AvailabilityController  extends TimerTask{
	
    /**
     * TimeTask run().
     * 
     * @return 
     */
    public void run() {
        System.out.println("Requesting server...PENDING");
        
        try {
			if(Configuration.checkFile()){
		        try {
		        	System.out.print("Serveur response...\b");
		        	
		        	if(this.checkAvaibility()) {
		        		// All fine
		        	}
		        	else {
		        		EmailController.sendNotRespondingMessage(Configuration.ADMINISTRATOR_EMAIL);
		        	}
		        	
		        	} 
		        catch (AddressException e) {e.printStackTrace();} 
		        catch (MessagingException e) {e.printStackTrace();}
			}
		} 
        catch (FileNotFoundException e1) {e1.printStackTrace();} 
        catch (IOException e1) {e1.printStackTrace();}
    }
    
    /**
     * Is he alive!? ...
     * then check validity of Mquestion object returned by JSON.
     * 
     * @return 
     */
    private boolean checkAvaibility() throws AddressException, MessagingException {

    	if(WebService.sendRequest()) {
            System.out.println("Requesting server...DONE");
            
            try{
                Gson gson = new Gson();
                Question[] questionsTemp = gson.fromJson(WebService.response, Question[].class);
                return true;
            	} catch (JsonParseException e){
            		EmailController.sendCouldNotGetJSONMessage(Configuration.ADMINISTRATOR_EMAIL);
            		return false;
            	}
    	}
    	else {
    		System.out.println("Requesting server...FAIL");
    		EmailController.sendNotRespondingMessage(Configuration.ADMINISTRATOR_EMAIL);
        	return false;
    	}
    }
}
