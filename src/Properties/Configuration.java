package Properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuration {

	public static String ADMINISTRATOR_EMAIL; 
	public static String WEBSERVICE_URL;
	
	public static boolean checkFile() throws FileNotFoundException, IOException {
	
		// Check properties file
				Properties propertiesFile = new Properties();
				 
				System.out.print("Loading properties file...\b");
				 
		    	try {
		    		propertiesFile.load(new FileInputStream("config.properties"));
		    		
		    		ADMINISTRATOR_EMAIL = propertiesFile.getProperty("ADMINISTRATOR_EMAIL");
		    		WEBSERVICE_URL = propertiesFile.getProperty("WEBSERVICE_URL");
		    		
		    		System.out.println("OK");
		    		
		    		return true;
		 
		    	} catch (IOException ex) {
		    		System.out.println("FAIL");
		    		System.out.println("Auto-generation of default config.properties file...");
		    		
		    		propertiesFile.setProperty("ADMINISTRATOR_EMAIL", "administrator@email.com");
		    		propertiesFile.setProperty("WEBSERVICE_URL", "http://your_webservice_url.com");
	
		    		propertiesFile.store(new FileOutputStream("config.properties"), null);
		    		
		    		return false;
		        }
	}
	
	
}
