import java.io.*;
import java.util.Timer;

import Avaibility.AvailabilityController;

/**
 * Main.java
 * Purpose: Main class.
 *
 * @author Tommy Vinhas
 * @version 1.0 05/12/2013
 */

public class Main {

    /**
     * Main class 
     * 
     * @param args Not used.
     * @return 
     */
	public static void main(String[] args)  throws IOException {

		System.out.println("############ Start tests ############");
    	
    	// Crontab
		Timer cron = new Timer();
		cron.schedule(new AvailabilityController(), 0, 100000);
	}
}
