package ReportServer;
import javax.mail.*;
import javax.mail.internet.*;

import java.util.Date;
import java.util.Properties;

/**
 * EmailController.java
 * Purpose: Just sends mails...
 *
 * @author Tommy Vinhas
 * @version 1.0 05/12/2013
 */

public class EmailController {
	
	private static final String HOST = "smtp.gmail.com";
	private static final String FROM = "adm.jeux.mtc@gmail.com";
	private static final String PASS = "aqzsedrftg";
	
    /**
     * Send custom message from gmail.com
     * 
     * @param content Mail content.
     * @param subject Mail subject.
     * @param administratorEmail Administrator e-mail address.
     * @return 
     */
	public static void sendCustomMessage(String content, String subject, String administratorEmail) throws AddressException, MessagingException
	{
		Properties props = System.getProperties();
		
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", HOST);
		props.put("mail.smtp.user", FROM);
		props.put("mail.smtp.password", PASS);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");
		
		Session session = Session.getDefaultInstance(props, null);
		
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(FROM));
		message.addRecipient(Message.RecipientType.TO,  new InternetAddress(administratorEmail));
		message.setSubject(subject);
		message.setContent(content, "text/html");
		
		Transport transport = session.getTransport("smtp");
		transport.connect(HOST, FROM, PASS);
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();
	}

    /**
     * Send "not responding" message from gmail.com
     * 
     * @param administratorEmail Administrator e-mail address.
     * @return 
     */
	public static void sendNotRespondingMessage(String administratorEmail) throws AddressException, MessagingException
	{
		Properties props = System.getProperties();
		
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", HOST);
		props.put("mail.smtp.user", FROM);
		props.put("mail.smtp.password", PASS);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");
		
		Session session = Session.getDefaultInstance(props, null);
		
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(FROM));
		message.addRecipient(Message.RecipientType.TO,  new InternetAddress(administratorEmail));
		message.setSubject("[CRASH REPORT]");
		message.setContent("The Webservice is not responding to HTTP GET request.", "text/html");
		message.setSentDate(new Date());
		
		Transport transport = session.getTransport("smtp");
		transport.connect(HOST, FROM, PASS);
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();
	}
	
	/**
     * Send "not responding" message from gmail.com
     * 
     * @param administratorEmail Administrator e-mail address.
     * @return 
     */
	public static void sendCouldNotGetJSONMessage(String administratorEmail) throws AddressException, MessagingException
	{
		Properties props = System.getProperties();
		
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", HOST);
		props.put("mail.smtp.user", FROM);
		props.put("mail.smtp.password", PASS);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");
		
		Session session = Session.getDefaultInstance(props, null);
		
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(FROM));
		message.addRecipient(Message.RecipientType.TO,  new InternetAddress(administratorEmail));
		message.setSubject("[CRASH REPORT]");
		message.setContent("Test services are not able to deserialize JSON from webservice.", "text/html");
		message.setSentDate(new Date());
		
		Transport transport = session.getTransport("smtp");
		transport.connect(HOST, FROM, PASS);
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();
	}
}